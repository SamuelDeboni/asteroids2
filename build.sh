#!/bin/bash

if [[ $1 = "run" ]] ; then
    gcc -o ast2 src/main.c lib/libraylib.a -lGL -lm -lpthread -ldl -lrt -lX11 -Iinclude && ./ast2
else
    gcc -o ast2 src/main.c lib/libraylib.a -lGL -lm -lpthread -ldl -lrt -lX11 -Iinclude
fi
