#ifndef ENTITY_H
#define ENTITY_H

#include "base/base_types.h"
#include "raymath.h"

typedef struct EntityHandle {
    u16 id, gen;
} EntityHandle;

typedef enum EntityFlag {
    EntityFlag_DESTROY = 1,
    EntityFlag_STATIC = 2,
} EntityFlag;

typedef struct Entity {
    EntityHandle handle;
    
    u32 flags;
    Vector3 position;
    Vector3 velocity;
    f32 rotation;
    Texture texture;
    
    // Free list indices
    u16 next_free; 
} Entity;


#define MAX_ENTITIES 512
typedef struct EntityManager {
    Entity entities[MAX_ENTITIES];
    u32 last_entity;

    u16 first_free;
} EntityManager;

function void set_entity_manager(EntityManager *manager);

function Entity *entity_ptr(EntityHandle handle);
function Entity *create_entity();
function void destroy_entity(EntityHandle handle);

function void update_entities();

#endif // !ENTITY_H
