#include "entity.h"

static EntityManager *_em;

function void
set_entity_manager(EntityManager *entity_manager) { _em = entity_manager; }

function Entity *
entity_ptr(EntityHandle handle)
{
    ASSERT(_em);
    Entity *ptr = &_em->entities[0];

    if (handle.id >= MAX_ENTITIES || handle.id > _em->last_entity) {
        return ptr;
    }

    if (_em->entities[handle.id].handle.id == handle.id && 
        _em->entities[handle.id].handle.gen == handle.gen) 
    {
        ptr = &_em->entities[handle.id];
    }

    return ptr;
}

function Entity *
create_entity()
{
    ASSERT(_em);
    
    u16 id = 0;
    if (!_em->first_free) {
        ASSERT(_em->first_free < MAX_ENTITIES);

        id = _em->first_free;
        _em->first_free = _em->entities[_em->first_free].next_free;
    } else if (_em->last_entity < MAX_ENTITIES - 1) {
        id = ++_em->last_entity;
    }

    Entity *ptr = &_em->entities[id];
    *ptr = (Entity){
        .handle = (EntityHandle){id, ptr->handle.gen + 1},
    };

    return ptr;
}

function void 
destroy_entity(EntityHandle handle)
{
    ASSERT(_em);
    
    Entity *ptr = entity_ptr(handle);
    if (!ptr->handle.id) {
        ptr->next_free = _em->first_free;
        ptr->handle.id = 0;
        _em->first_free = ptr->handle.id;
    }
}


function void 
update_entities()
{
    for (int idx = 0; idx < _em->last_entity; idx++) {
        Entity *e = &_em->entities[idx];
        if (e->handle.id == 0) continue;
        
        if ((~e->flags) & EntityFlag_STATIC) {
            e->position = Vector3Scale(Vector3Add(e->position, e->velocity), delta_time);
        }

    }
}
