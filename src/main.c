#include "base/base_types.h"
#include "base/base_mem.h"

#include "raylib.h"
#include "rlgl.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"

#define WIN_W 1280 
#define WIN_H 720

#include "entity.h"

// === Globals ===
u64 frame_counter;
M_Arena *game_arena;
M_Arena *frame_arena;

f32 delta_time;

EntityManager *game_entity_manager;

Camera camera;

// === Unity Builds ===
#include "entity.c"

int 
main(void)
{
    InitWindow(WIN_W, WIN_H, "asteroids 2");
    SetTargetFPS(60);

    // === Init Arenas ===
    game_arena = m_create_arena(MEGABYTE(64));
    frame_arena = m_create_arena(MEGABYTE(32));
    
    // === Init Entity Manager ===
    game_entity_manager = PUSH_ARRAY(game_arena, EntityManager, 1);
    set_entity_manager(game_entity_manager); 

    // === Setup Camera ===
    camera.position = (Vector3){0.0f, 0.0f, 0.0f};
    camera.target = (Vector3){0.0f, 0.0f, 50.0f};
    camera.up = (Vector3){0.0f, 1.0f, 0.0f};
    camera.fovy = 70.0f;
    camera.projection = CAMERA_PERSPECTIVE;

    Texture ast_texture = LoadTexture("assets/Textures/BaseContourn.png");

    Model skybox;
    {
        Mesh sky_mesh = GenMeshCube(1.0f, 1.0f, 1.0f);
        skybox = LoadModelFromMesh(sky_mesh);
        skybox.materials[0].shader = LoadShader(
                TextFormat("assets/shaders/skybox.vs", RLGL_VERSION),
                TextFormat("assets/shaders/skybox.fs", RLGL_VERSION)
                );

        SetShaderValue(skybox.materials[0].shader, GetShaderLocation(skybox.materials[0].shader, "cubemap"), (int[1]){MATERIAL_MAP_CUBEMAP}, SHADER_UNIFORM_INT);

        Image img = LoadImage("assets/Textures/Skybox2.png");
        skybox.materials[0].maps[MATERIAL_MAP_CUBEMAP].texture = LoadTextureCubemap(img, CUBEMAP_LAYOUT_CROSS_FOUR_BY_THREE);
        UnloadImage(img);
    }

    DisableCursor();

    Model ast_med = LoadModel("assets/models/Asteroids_Sml.obj");
    ast_med.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = ast_texture;

    while (!WindowShouldClose())
    {
        delta_time = GetFrameTime();
        UpdateCamera(&camera, CAMERA_FREE);

        BeginDrawing();
        ClearBackground(WHITE);

        BeginMode3D(camera);

        rlDisableBackfaceCulling();
        rlDisableDepthMask();
        DrawModel(skybox, (Vector3){0, 0, 0}, 1.0f, WHITE);
        rlEnableBackfaceCulling();
        rlEnableDepthMask();

        DrawModel(ast_med, (Vector3){0.0f, 0.0f, 50.0f}, 1.0f, WHITE);
        DrawModel(ast_med, (Vector3){10.0f, 1.0f, 50.0f}, 1.0f, WHITE);
        DrawModel(ast_med, (Vector3){5.0f, 10.0f, 20.0f}, 1.0f, WHITE);

        EndMode3D();
        

        EndDrawing();

        frame_counter++;
    }

    CloseWindow();

    return 0;
}


#include "base/base_mem.c"
