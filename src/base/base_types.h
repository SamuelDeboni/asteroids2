#ifndef BASE_TYPES_H
#define BASE_TYPES_H

//~ Context Macros
#if defined(__clang__)
# define COMPILER_CLANG 1

# if defined(_WIN32)
#  define OS_WINDOWS 1
# elif defined(__gnu_linux__)
#  define OS_LINUX 1
# elif defined(__APPLE__) && defined(__MACH__)
#  define OS_MAC 1
# else
#  define OS_WASM
# endif

# if defined(__amd64__)
#  define ARCH_X64 1
// TODO: verify this works on clang
# elif defined(__i386__)
#  define ARCH_X86 1
// TODO: verify this works on clang
# elif defined(__arm__)
#  define ARCH_ARM 1
// TODO: verify this works on clang
# elif defined(__aarch64__)
#  define ARCH_ARM64 1
# else
#  define ARCH_WASM
# endif

#elif defined(_MSC_VER)
# define COMPILER_CL 1

# if defined(_WIN32)
#  define OS_WINDOWS 1
# else
#  error missing OS detection
# endif

# if defined(_M_AMD64)
#  define ARCH_X64 1
# elif defined(_M_I86)
#  define ARCH_X86 1
# elif defined(_M_ARM)
#  define ARCH_ARM 1
// TODO: ARM64?
# else
#  error missing ARCH detection
# endif

#elif defined(__GNUC__)
# define COMPILER_GCC 1

# if defined(_WIN32)
#  define OS_WINDOWS 1
# elif defined(__gnu_linux__)
#  define OS_LINUX 1
# elif defined(__APPLE__) && defined(__MACH__)
#  define OS_MAC 1
# else
#  error missing OS detection
# endif

# if defined(__amd64__)
#  define ARCH_X64 1
# elif defined(__i386__)
#  define ARCH_X86 1
# elif defined(__arm__)
#  define ARCH_ARM 1
# elif defined(__aarch64__)
#  define ARCH_ARM64 1
# else
#  error missing ARCH detection
# endif

#else
# error no context cracking for this compiler
#endif

// Zero fill missing context macros
#if !defined(COMPILER_CL)
# define COMPILER_CL 0
#endif
#if !defined(COMPILER_CLANG)
# define COMPILER_CLANG 0
#endif
#if !defined(COMPILER_GCC)
# define COMPILER_GCC 0
#endif
#if !defined(OS_WINDOWS)
# define OS_WINDOWS 0
#endif
#if !defined(OS_LINUX)
# define OS_LINUX 0
#endif
#if !defined(OS_MAC)
# define OS_MAC 0
#endif
#if !defined(ARCH_X64)
# define ARCH_X64 0
#endif
#if !defined(ARCH_X86)
# define ARCH_X86 0
#endif
#if !defined(ARCH_ARM)
# define ARCH_ARM 0
#endif
#if !defined(ARCH_ARM64)
# define ARCH_ARM64 0
#endif

// ===== Usefull Macros =====
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define CLAMP(v, a, b) MAX((a), MIN((v), (b)))

#define CLAMP_TOP(v, t) (((v) < (t)) ? (v) : (t))
#define CLAMP_BOTTOM(v, b) (((v) > (b)) ? (v) : (b))

#define STMNT(s) do { s } while(0)

#if !defined(ASSERT_BREAK)
# define ASSERT_BREAK() (*(int *)0 = 0)
#endif

#if defined(ENABLE_ASSERT)
# define ASSERT(c) STMNT(if (!(c)) { ASSERT_BREAK(); })
#else
# define ASSERT(c)
#endif

//~ ===== Basic types =====
#include <stdint.h>

#define function   static
#define global     static
#define persistent static

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float  f32;
typedef double f64;

typedef uint8_t  b8;
typedef uint32_t b32;

typedef uintptr_t uptr;

typedef void VoidFunc(void);


#define true  1
#define false 0

//~ ===== Basic Constants =====

// TODO(samuel): Add basic constants
#define PI 3.141592653589793
#define HALF_PI 1.570796326794897
#define TAU 6.283185307179586
#define SIN_CURVE_A 0.0415896
#define SIN_CURVE_B 0.00129810625032

#define KILOBYTE(x) ((u64)(x) * (u64)1024L)
#define MEGABYTE(x) ((u64)KILOBYTE(x) * (u64)1024L)
#define GIGABYTE(x) ((u64)MEGABYTE(x) * (u64)1024L)

//~ Linked list macros

#define SLL_INSERT(l, n) \
if((n) && (l)) { \
(n)->next = (l)->next; \
(l)->next = (n); \
}\

#define SLL_REMOVE(l) \
if((l) && (l)->next) { \
(l)->next = (l)->next->next; \
}


#endif
