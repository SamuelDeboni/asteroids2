#include "base_mem.h"
#include "base_types.h"

#define M_ARENA_COMMIT_SIZE (1024 * 64)

#if OS_LINUX

#include <sys/mman.h>

function void *m_reserve(u64 size) {
	void *mem = mmap(0, size, PROT_NONE, MAP_PRIVATE | MAP_ANON, -1, 0);
    return mem;
}

function void m_commit(void *ptr, u64 size) {
	int res = mprotect(ptr, size, PROT_WRITE | PROT_READ);
    ASSERT(res == 0);
}

function void m_decommit(void *ptr, u64 size) {
	int res = mprotect(ptr, size, PROT_NONE);
    ASSERT(res == 0);
}

function void *m_alloc(u64 size) {
	void *mem = m_reserve(size);
    m_commit(mem, size);
    return mem;
}

function void m_free(void *ptr, u64 size) {
    munmap(ptr, size);
}
#endif

// TODO: windows mem implementation
#if OS_WINDOWS
#include <memoryapi.h>

function void *m_reserve(u64 size) {
	void *mem = VirtualAlloc(0, size, MEM_RESERVE, PAGE_READWRITE);
    return mem;
}

function void m_commit(void *ptr, u64 size) {
	void *res = VirtualAlloc(ptr, size, MEM_COMMIT, PAGE_READWRITE);
    ASSERT(res == 0);
}

function void m_decommit(void *ptr, u64 size) {
    VirtualFree(ptr, size, MEM_DECOMMIT);
    ASSERT(res == 0);
}

function void *m_alloc(u64 size) {
	void *mem = VirtualAlloc(0, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return mem;
}

function void m_free(void *ptr, u64 size) {
    VirtualFree(ptr, size, MEM_RELEASE | MEM_DECOMMIT);
}
#endif

function M_Arena *
m_create_arena_mem(void *mem, u64 capacity)
{
	M_Arena *arena = (M_Arena *)mem;
	arena->reserved    = capacity;
	arena->commited    = capacity;
	arena->offset      = sizeof(M_Arena);
	arena->prev_offset = arena->offset;
	return arena;
}

function u64
m_align_forward(u64 ptr, u64 align)
{
	u64 p = ptr;
	u64 a = (u64)align;
	u64 modulo = p & (a - 1);

	if (modulo != 0) {
		p += a - modulo;
	}

	return p;
}

function M_Arena *
m_create_arena(u64 capacity)
{
    capacity = capacity + sizeof(M_Arena);
    capacity = m_align_forward(capacity, M_ARENA_COMMIT_SIZE);

	void *mem = m_reserve(capacity);
    m_commit(mem, M_ARENA_COMMIT_SIZE);

	M_Arena *arena     = (M_Arena *)mem;
	arena->reserved    = capacity;
	arena->commited    = M_ARENA_COMMIT_SIZE;
	arena->offset      = sizeof(M_Arena);
	arena->prev_offset = arena->offset;

	return arena;
}

function void
m_arena_release(M_Arena *arena)
{
	ASSERT(arena);
	m_free((void *)arena, arena->reserved);
}

function void
m_arena_reset(M_Arena *arena)
{
	ASSERT(arena);
	arena->offset      = sizeof(M_Arena);
	arena->prev_offset = arena->offset;
}


function void *
m_arena_push(M_Arena *arena, u64 size)
{
    return m_arena_push_align(arena, size, 1);
}

function void *
m_arena_push_align(M_Arena *arena, u64 size, u64 align)
{
	void *result = 0;

	u64 offset = (u64)arena->offset;
	//offset = m_align_forward(offset, align);

    while (offset + size > arena->commited) {
        if (arena->commited + M_ARENA_COMMIT_SIZE > arena->reserved) {
            return 0;
        }

        m_commit(((u8 *)arena) + arena->commited, M_ARENA_COMMIT_SIZE);
        arena->commited += M_ARENA_COMMIT_SIZE;
    }

	if (offset + size <= arena->commited) {
		result = (u8 *)arena + offset;
		arena->prev_offset = arena->offset;
		arena->offset = offset + size;
		MEMSET((u8 *)result, 0, size);
	}

	return result;
}

function void *
m_arena_mem_start(M_Arena *arena) {
	return (void*)(arena + 1);
}

function void *
m_arena_current_ptr(M_Arena *arena) {
	return (u8*)arena + arena->offset;
}

function void
m_arena_pop(M_Arena *arena)
{
	arena->offset = arena->prev_offset;
}

function void
m_arena_pop_to(M_Arena *arena, u64 offset)
{
	arena->offset = m_align_forward(offset + (u64)arena, M_DEFAULT_ALIGN) - (u64)arena;
}

function void
m_arena_pop_count(M_Arena *arena, u64 count)
{
	m_arena_pop_to(arena, arena->offset - count);
}
