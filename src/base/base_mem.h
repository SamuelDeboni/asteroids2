#ifndef ARENAS_H
#define ARENAS_H

#include "base_types.h"

typedef struct M_Arena M_Arena;
struct M_Arena {
	u64 reserved;
	u64 commited;
	u64 offset;
	u64 prev_offset;
};

#define M_DEFAULT_ALIGN 16
#define MEMSET(m, v, l) for(u64 __i = 0; __i < (l); __i++) {(m)[__i] = (v);}
#define PUSH_ARRAY(a, T, c) (T*)m_arena_push((a), sizeof(T)*(c))
#define PUSH_ARRAY_ALIGN(a, T, c, al) (T*)m_arena_push_align((a), sizeof(T)*(c), (al))

function M_Arena *m_create_arena(u64 capacity);
function M_Arena *m_create_arena_mem(void *mem, u64 capacity);
function void m_arena_release(M_Arena *arena);
function void m_arena_reset(M_Arena *arena);
function void * m_arena_push_align(M_Arena *arena, u64 size, u64 align);
function void *m_arena_push(M_Arena *arena, u64 size);
function void *m_arena_mem_start(M_Arena *arena);
function void *m_arena_current_ptr(M_Arena *arena);
function void m_arena_pop(M_Arena *arena);
function void m_arena_pop_to(M_Arena *arena, u64 offset);
function void m_arena_pop_count(M_Arena *arena, u64 count);

// TODO(samuel): add the ability to only commit when needed

#endif // ARENAS
