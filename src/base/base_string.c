#include "base_string.h"

function String8
str8(u8 *buffer, u64 len)
{
	String8 result = {buffer, len};
	//buffer[len] = 0;

	return result;
}

function String8
str8_cstr(char *cstr)
{
	String8 result = {(u8 *)cstr, 0};
	char *p = cstr;
	while (*(p++)) result.len++;
	return result;
}

function String8
str8_trim(String8 str) {
	String8 result = str;

	for (;result.len > 0 && result.str[0] == ' ';) {
		str8_offset(&result, 1);
	}

	for (;result.len > 0 && result.str[result.len - 1] == ' ';) {
		result.len--;
	}

	return result;
}

function b32
str8_equals(String8 s1, String8 s2)
{
	if (s1.len != s2.len) {
		return false;
	}

	if (s1.str == s2.str) {
		return true;
	}

	for (u64 i = 0; i < s1.len; i++) {
		if (s1.str[i] != s2.str[i]) {
			return false;
		}
	}

	return true;
}

function String8
str8_copy(M_Arena *arena, String8 str)
{
	u8 *r_buffer = PUSH_ARRAY(arena, u8, str.len + 1);
	for (u64 i = 0; i < str.len; i++) {
		r_buffer[i] = str.str[i];
	}
	return str8(r_buffer, str.len);
}


function String8
str8_substr(String8 str, u64 start_idx, u64 end_idx)
{
	String8 result = {0};
	end_idx = CLAMP(end_idx, start_idx, str.len);
	ASSERT(start_idx < str.len);

	result.len = end_idx - start_idx;
	result.str = str.str + start_idx;

	return result;
}

function String8List
str8_split(M_Arena *arena, String8 str, u8 separator)
{
	String8List list = {0};

	u64 start_idx = 0;
	for (u64 i = 0; i < str.len; i++) {
		if (str.str[i] == separator) {
			String8 sub_str = str8_substr(str, start_idx, i);
			if (sub_str.len > 0) {
				str8_push_back(arena, &list, sub_str);
			}
			start_idx = i+1;
		}
	}

	String8 sub_str = str8_substr(str, start_idx, str.len);
	if (sub_str.len > 0) {
		str8_push_back(arena, &list, sub_str);
	}

	return list;
}

function void
str8_push_back(M_Arena *arena, String8List *list, String8 str)
{
	if (list->len == 0) {
		String8Node *new_node = PUSH_ARRAY(arena, String8Node, 1);
		new_node->str = str;
		list->first = new_node;
		list->last  = new_node;
		list->len += 1;
	} else if (list->last) {
		String8Node *new_node = PUSH_ARRAY(arena, String8Node, 1);
		new_node->str = str;
		new_node->prev = list->last;
		new_node->prev->next = new_node;
		list->last = new_node;
		list->len += 1;
	}
}

function String8
str8_pop_back(M_Arena *arena, String8List *list)
{
	String8 result = {0};
	if (list->last) {
		result = list->last->str;
		list->last = list->last->prev;
		list->len -= 1;
	}
	return result;
}

function void
str8_push_front(M_Arena *arena, String8List *list, String8 str)
{
	if (list->len == 0) {
		String8Node *new_node = PUSH_ARRAY(arena, String8Node, 1);
		new_node->str = str;
		list->first = new_node;
		list->last  = new_node;
		list->len += 1;
	} else if (list->first) {
		String8Node *new_node = PUSH_ARRAY(arena, String8Node, 1);
		new_node->str = str;
		new_node->next = list->first;
		new_node->next->prev = new_node;
		list->first = new_node;
		list->len += 1;
	}
}

function String8
str8_pop_front(M_Arena *arena, String8List *list)
{
	String8 result = {0};
	if (list->first) {
		result = list->first->str;
		list->first = list->first->next;
		list->len -= 1;
	}
	return result;
}

// TODO(samuel): Check for out of bound senarios
function b32
str8_to_i64_decimal(String8 str, i64 *out)
{
	b32 ok = true;
	str = str8_trim(str);

	i64 factor = 1;
	for (i64 i = str.len - 1; i >= 0; i--) {
		u8 c = str.str[i];

		if (c >= '0' && c <= '9') {
			*out += (i64)(c - '0') * factor;
			factor *= 10;
		} else if (i == 0 && c == '-') {
			*out = - *out;
		} else {
			*out = 0;
			ok = false;
			break;
		}
	}

	return ok;
}

function b32
str8_to_f64(String8 str, f64 *out)
{
	str = str8_trim(str);

	b32 ok = true;
	b32 after_coma = false;
	u32 digit_count = 0;
	*out = 0;

	b32 is_negative = false;
	if (str.str[0] == '-') {
		is_negative = true;
	}

	for (u64 i = 0; i < str.len; i++) {
		u8 c = str.str[i];

		if (c >= '0' && c <= '9') {
			*out *= 10;
			*out += (f64)(c - '0');
			digit_count += after_coma;

		} else if (c == '.' && !after_coma) {
			after_coma = true;
		} else {
			*out = 0;
			ok = false;
			break;
		}
	}

	if (ok) for (u64 i = 0; i < after_coma; i++) {
		*out /= 10.0;
	}

	return ok;
}